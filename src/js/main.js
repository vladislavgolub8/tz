let elem;
let elemContent;

window.onload = function () {
    elemContent = document.getElementsByClassName('activeContent');
    elem = document.getElementsByClassName('offerBox__icon');
    hideContent(1);
}
function hideContent(a) {
    for (let i = a;i < elemContent.length;i++) {
        elemContent[i].classList.remove('activeContent_show');
        elemContent[i].classList.add("activeContent_hide");
        elem[i].classList.remove('offerBox__icon_bgGreen');

    }
}
document.getElementById('windowActive').onclick = function (event) {
    let target = event.target;
    if (target.className == 'offerBox__icon') {
        for (let i = 0;i < elem.length;i++) {
            if (target == elem[i]) {
                showContent(i);
                break;
            }
        }
    }
}
function showContent(b) {
    if (elemContent[b].classList.contains('activeContent_hide')) {
        hideContent(0);
        elem[b].classList.add('offerBox__icon_bgGreen');
        elemContent[b].classList.remove('activeContent_hide');
        elemContent[b].classList.add('activeContent_show');
    }
}




let button = document.getElementById('menuToggle');
let menu = document.querySelector('.menu');

button.onclick = () => { 
   menu.classList.toggle('menu_active');
}